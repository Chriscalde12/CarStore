'use strict'

const app = require('./app');
const mongoose = require('mongoose');

let uri= 'mongodb://admin:12345@ds247648.mlab.com:47648/carstore'

const port = process.env.PORT || 3000;

mongoose.connect(uri,(err,res)=>{
    if(err){
        throw (err)
    }else{
        console.log("Succesfull Connection to MongoDB");
        app.listen(port,function () {
            console.log(`API REST FAVORITOS FUNCIONANDO en ${port}`);
        });
    }
});

let db = mongoose.connection;
db.on('error', console.error.bind(console,'connection error:'));