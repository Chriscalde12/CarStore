var Client = require('../models/client-model');
var Car = require('../models/car-model');

function loadCreateClient(req,res) {
    Car.find({}, function (err, cat) {
            if (err) {
                res.status(500).send({message: "Error al guardar"})
            }
            res.status(200).render('client/create', {name: cat});
        }
    );
}

function loadEditClient(req,res){
    var categoryId = req.params.id;
    Car.find({}, function (err,cat){
        if(err){
            res.status(500).send({message:"Error al guardar"})
        }
        Client.findById(categoryId, function (err, posts) {
            res.status(200).render('client/edit',
                {
                    name:cat,
                    clients: posts
                }
            );
        });
    });

}

function createClient(req,res) {
    var client = new Client();
    var params = req.body;

    client.cname = params.cname;
    client.age = params.age;
    client.name = params.name;
    if (client.name != 0) {
        client.save((err, carStored) => {
            if (err) {
                res.status(500).send({message: "Error al Guardar"})
            }
            res.redirect('./list/1')
        });
    }
}

function readClient(req,res){
    var pg = req.params.pg;
    Client.paginate({},{page:pg, limit:6},function (err,posts){
        if(err){
            res.status(500).send({message:"ERROR al regresar usuarios"})
        }
        if(!posts){
            res.status(404).send({message:"No se encontro ningun dato m8"})
        }
    })
        .then(response =>{
            res.status(200).render('client/list', {clients:response.docs, pagina: response.pages, active: response.page})
        });
}
function editClient(req,res){
    var userId= req.params.id;
    var updateInfo = req.body;

    Client.findByIdAndUpdate(userId,updateInfo,function (err,users) {
        if(!users){
            res.status(404).send("No se encontro el id");
        }
        if (err){
            res.status(500).send("ERROR DEL SERVIDOR");
        }
        res.redirect('../list/1');
    });
}

function deleteClient(req,res){
    var userId= req.params.id;
    Client.findOneAndRemove({_id: userId}, function (err) {
        if(err){
            res.status(500).send("Error del Servidor")
        }
        res.redirect('../list/1')
    });
}

module.exports= {
    loadCreateClient,
    loadEditClient,
    createClient,
    readClient,
    editClient,
    deleteClient

}