var Car = require('../models/car-model');


function loadCreateCar(req,res){
    res.status(200).render('car/create');
}

function loadEditCar(req,res){
    var userId = req.params.id;
    Car.findById(userId,function (err,posts) {
        res.status(200).render('car/edit',{title: "Update usuario", cars: posts});
    });
}

function createCar(req,res){
    var car = new Car();
    var params = req.body;

    car.name = params.name;
    car.year = params.year;
    car.description = params.description;
    car.save((err,carStored) =>{
        if(err){
            res.status(500).send({message:"Error al Guardar"})
        }
        res.redirect('./list/1')
    });
}

function readCar(req,res){
    var pg = req.params.pg;
    Car.paginate({},{page:pg, limit:6},function (err,posts){
        if(err){
           res.status(500).send({message:"ERROR al regresar usuarios"})
        }
        if(!posts){
           res.status(404).send({message:"No se encontro ningun dato m8"})
        }
    })
        .then(response =>{
            res.status(200).render('car/list', {cars:response.docs, pagina: response.pages, active: response.page})
    });
}
function editCar(req,res){
    var userId= req.params.id;
    var updateInfo = req.body;

    Car.findByIdAndUpdate(userId,updateInfo,function (err,users) {
        if(!users){
            res.status(404).send("No se encontro el id");
        }
        if (err){
            res.status(500).send("ERROR DEL SERVIDOR");
        }
        res.redirect('../list/1');
    });
}

function deleteCar(req,res){
    var userId= req.params.id;
    Car.findOneAndRemove({_id: userId}, function (err) {
        if(err){
            res.status(500).send("Error del Servidor")
        }
        res.redirect('../list/1')
    });
}

module.exports= {
    loadCreateCar,
    loadEditCar,
    createCar,
    readCar,
    editCar,
    deleteCar

}