var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var car  = require('./routes/car-route');
var client = require('./routes/client-route');

app.set('view engine','pug');

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/node_modules/bootstrap/dist'));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use('/car',car);
app.use('/client',client);

module.exports= app;