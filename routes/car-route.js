'use strict'

var express = require('express');
var carController = require('../controllers/car-controller');
var router = express.Router();

router.get('/create', carController.loadCreateCar);
router.get('/edit/:id',carController.loadEditCar);
router.get('/list/:pg',carController.readCar);
router.post('/create',carController.createCar);
router.post('/edit/:id',carController.editCar);
router.get('/delete/:id',carController.deleteCar);

module.exports = router;