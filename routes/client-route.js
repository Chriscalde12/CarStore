'use strict'

var express = require('express');
var clientController = require('../controllers/client-controller');
var router = express.Router();

router.get('/create', clientController.loadCreateClient);
router.get('/edit/:id',clientController.loadEditClient);
router.get('/list/:pg',clientController.readClient);
router.post('/create',clientController.createClient);
router.post('/edit/:id',clientController.editClient);
router.get('/delete/:id',clientController.deleteClient);

module.exports = router;