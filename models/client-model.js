'use strict'
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var Schema = mongoose.Schema;

var ClientSchema = Schema({
    cname: String,
    age: String,
    name: String
});

ClientSchema.plugin(mongoosePaginate);

module.exports= mongoose.model('client',ClientSchema);