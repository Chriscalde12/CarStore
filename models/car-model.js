'use strict'
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var Schema = mongoose.Schema;

var CarSchema = Schema({
    name: String,
    year: String,
    description: String
});

CarSchema.plugin(mongoosePaginate);

module.exports= mongoose.model('car',CarSchema);